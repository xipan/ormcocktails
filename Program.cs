using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;


namespace Cocktails
{
    class Program
    {
        static void Main(string[] args)
        {
            // The code provided will print ‘Hello World’ to the console.
            // Press Ctrl+F5 (or go to Debug > Start Without Debugging) to run your app.
            Console.WriteLine("Hello World!");

            Console.WriteLine("Enter the action code(1:create, 2:search for drinks types, 3:del):");
            string caseSwitch = Console.ReadLine();

            CocktailsHelper cocktailsHelper = new CocktailsHelper();

            switch (caseSwitch)
            {
                case "1": 
                    // Create cocktails start
                    Console.WriteLine("Cocktailst Name:");
                    string cocktailstName = Console.ReadLine();
                    Console.WriteLine("Cocktailst Memo:");
                    string cocktailstMemo = Console.ReadLine();

                    bool isLoop = true;
                    string getLoop = "n";
                    string itemFormulaName;
                    decimal itemFormulaCapacity;
                    List<CocktailsFormula> cocktailsFormulas = new List<CocktailsFormula>();
                    do
                    {
                        Console.WriteLine("Formulas Name:");
                        itemFormulaName = Console.ReadLine();
                        Console.WriteLine("Formulas Capacity:");
                        itemFormulaCapacity = decimal.Parse(Console.ReadLine());

                        cocktailsFormulas.Add(new CocktailsFormula { ItemName = itemFormulaName, Capacity = itemFormulaCapacity });

                        Console.WriteLine("Do you want to continue?(y/n)");
                        getLoop = Console.ReadLine();
                        isLoop = (getLoop == "y") ? true : false;
                    } while (isLoop);
                    cocktailsHelper.CreateCocktails(cocktailstName, cocktailstMemo, cocktailsFormulas);
                    // Create cocktails end                    
                    break;
                case "2":
                    //search for drinks types start
                    Console.WriteLine("drinks types:");
                    string formulasName = Console.ReadLine();

                    List<CocktailsList> cocktailsResualt = cocktailsHelper.GetAllCocktails();
                    List<int> searchResualt = cocktailsHelper.SearchForDrinksType(formulasName);
                    int isInSearchResualt;

                    foreach (var item in cocktailsResualt)
                    {
                        isInSearchResualt  = Array.IndexOf(searchResualt.ToArray(), item.CocktailsListId);
                        if (isInSearchResualt > -1)
                        {
                            Console.WriteLine(item.CocktailstName.ToString());
                            foreach(var itemFormula in item.CocktailsFormula)
                            {
                                Console.Write("{0}({1}ml)  ", itemFormula.ItemName.ToString(), itemFormula.Capacity.ToString());
                            }
                        }
                        Console.WriteLine(" ");
                    }
                    Console.ReadLine();
                    //search for drinks types end
                    break;
                case "3":
                    //del start
                    Console.WriteLine("List:");
                    List<CocktailsList> cocktailsAllResualt = cocktailsHelper.GetAllCocktails();

                    foreach (var item in cocktailsAllResualt)
                    {
                        Console.WriteLine("No.:{0},{1}",item.CocktailsListId.ToString(),item.CocktailstName.ToString());
                        foreach (var itemFormula in item.CocktailsFormula)
                        {
                            Console.Write("{0}({1}ml)  ", itemFormula.ItemName.ToString(), itemFormula.Capacity.ToString());
                        }
                        Console.WriteLine(" ");
                    }
                    Console.WriteLine("input No.:");
                    string delItem = Console.ReadLine();
                    cocktailsHelper.DelCocktails(int.Parse(delItem));
                    Console.ReadLine();
                    //del end
                    break;
                default:
                    Console.WriteLine("Quit!");
                    Console.ReadLine();
                    return;
            }


            return;
            //The following is a comment
            using (var db = new CocktailsContext())
            {                                
                var cocktails = new CocktailsList { CocktailstName  = "Margarita", CocktailstMemo = "abc"};
                var cocktailsFormula = new CocktailsFormula { ItemName = "Lime Juic", Capacity = 60M, ItemMemo = "abctest" };
                cocktails.CocktailsFormula = new List<CocktailsFormula>() {
                    new CocktailsFormula { ItemName = "Lime Juic", Capacity = 60M },
                    new CocktailsFormula { ItemName = "Triple Sec", Capacity = 30M },
                    new CocktailsFormula { ItemName = "Tequila", Capacity = 60M },
                 };

                db.CocktailsLists.Add(cocktails);
                db.SaveChanges();

            }
            Console.WriteLine("end");
            Console.ReadKey();
        }
    }

    public class CocktailsContext : DbContext
    {
        public CocktailsContext() : base("name=CocktailsContext")
        {
            Database.SetInitializer(new MigrateDatabaseToLatestVersion<CocktailsContext, Cocktails.Migrations.Configuration>());
        }
        public DbSet<CocktailsList> CocktailsLists { get; set; }
        public DbSet<CocktailsFormula> CocktailsFormulas { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            // Configure CocktailsList & CocktailsFormula entity
            //one to many
            modelBuilder.Entity<CocktailsFormula>() //many
                .HasRequired<CocktailsList>(sc => sc.CocktailsList)//many
                .WithMany(s => s.CocktailsFormula)//one
                .HasForeignKey<int>(sc => sc.CocktailsId);//many

        }
    }

    public class CocktailsList
    {
        public int CocktailsListId { get; set; }

        public string CocktailstName { get; set; }

        public string CocktailstMemo { get; set; }

        public ICollection<CocktailsFormula> CocktailsFormula { get; set; }


    }

    public class CocktailsFormula
    {
        public int CocktailsFormulaId { get; set; }

        /*
        [MySqlCharsetAttribute("latin")]
        */
        public string ItemName { get; set; }
        public decimal Capacity { get; set; }

        public int CocktailsId { get; set; }
        public string ItemMemo { get; set; }

        public virtual CocktailsList CocktailsList { get; set; }
    }

    public class CocktailsHelper
    {

        public void CreateCocktails(string cocktailstName, string cocktailstMemo, List<CocktailsFormula> cocktailsFormulas)
        {
            using (var db = new CocktailsContext())
            {
                var cocktails = new CocktailsList { CocktailstName = cocktailstName, CocktailstMemo = cocktailstMemo };
                cocktails.CocktailsFormula = cocktailsFormulas;
                db.CocktailsLists.Add(cocktails);
                db.SaveChanges();
            }

        }

        public List<int> SearchForDrinksType(string formulasName)
        {
            List<int> cocktailsList = new List<int>();
            using (var db = new CocktailsContext())
            {
                var cocktailsFromLinqLike = from c in db.CocktailsLists
                                            join f in db.CocktailsFormulas
                                            on c.CocktailsListId equals f.CocktailsId
                                            where f.ItemName.ToLower().Contains(formulasName)
                                            group c by c.CocktailsListId into g                     
                                            select new
                                            { CocktailsListId = g.Key};

                foreach (var item in cocktailsFromLinqLike)
                {
                    cocktailsList.Add(item.CocktailsListId);
                }                
            }

            return cocktailsList;
        }

        public List<CocktailsList> GetAllCocktails()
        {
            using (var db = new CocktailsContext())
            {
                var cocktailsFromLinqLike = (from c in db.CocktailsLists
                                            select c).Include("CocktailsFormula");
                return cocktailsFromLinqLike.ToList();
            }            

        }

        public void DelCocktails(int id)
        {
            using (var db = new CocktailsContext())
            {
                var cocktailsFromLinqLike = (from c in db.CocktailsLists
                                             where (c.CocktailsListId == id)
                                             select c).Include("CocktailsFormula").First();
                db.CocktailsLists.Remove(cocktailsFromLinqLike);
                db.SaveChanges();
            }

        }
    }


}
